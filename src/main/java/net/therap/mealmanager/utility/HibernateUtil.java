package net.therap.mealmanager.utility;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * @author arafat
 * @since 11/17/16
 */
public class HibernateUtil {

    private static SessionFactory sessionAnnotationFactory;

    public static void buildSessionAnntationFactory(){
        try{

            Configuration configuration = new Configuration();
            configuration.configure("hibernate-annotation.cfg.xml");

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().
                    applySettings(configuration.getProperties()).build();

            sessionAnnotationFactory = configuration.buildSessionFactory(serviceRegistry);

        }catch(Throwable ex){
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionAnnotationFactory() {
        if(sessionAnnotationFactory == null) {
           buildSessionAnntationFactory();
        }
        return sessionAnnotationFactory;
    }

    public static void closeSessionFactory(){
        sessionAnnotationFactory.close();
    }

}
