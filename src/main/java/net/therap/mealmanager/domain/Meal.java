package net.therap.mealmanager.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author arafat
 * @since 11/14/16
 */
@Entity
@Table(name = "meal")
public class Meal implements Serializable {

    private static final long serialVersionUId = 1L;

    @Id
    @Column(name = "meal_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "day_id")
    private WeekDay day;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "meal_type_id")
    private MealType type;

    @ManyToMany(targetEntity = Dish.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "meal_dish", joinColumns = {@JoinColumn(name = "meal_id")},
            inverseJoinColumns = {@JoinColumn(name = "dish_id")})
    private List<Dish> listOfDishes;

    public Meal(){

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public WeekDay getDay() {
        return day;
    }

    public void setDay(WeekDay day) {
        this.day = day;
    }

    public MealType getType() {
        return type;
    }

    public void setType(MealType type) {
        this.type = type;
    }

    public List<Dish> getListOfDishes() {
        return listOfDishes;
    }

    public void setListOfDishes(List<Dish> listOfDishes) {
        this.listOfDishes = listOfDishes;
    }
}
