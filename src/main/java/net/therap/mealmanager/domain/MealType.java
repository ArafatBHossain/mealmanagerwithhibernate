package net.therap.mealmanager.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author arafat
 * @since 11/13/16
 */
@Entity
@Table(name = "meal_type")
public class MealType implements Serializable {

    private static final long serialVersionUId = 1L;

    @Id
    @Column(name = "meal_type_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "meal_type_name")
    private String name;

    public MealType(){

    }

    public MealType(String name){
        this.setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int mealTypeId) {
        this.id = mealTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String mealTypeName) {
        this.name = mealTypeName;
    }

}
