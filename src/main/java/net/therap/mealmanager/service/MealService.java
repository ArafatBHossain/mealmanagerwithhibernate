package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.MealDao;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;

/**
 * @author arafat
 * @since 11/20/16
 */
public class MealService {

    MealDao mealDao;

    public MealService(){
        mealDao = new MealDao();
    }

    public Meal getMealById(int id){
        return mealDao.findById(id);
    }

    public void updateMeal(Meal meal){
        mealDao.update(meal);
    }

    public void saveMeal(Meal meal){
        mealDao.save(meal);
    }

    public Meal findMealByTypeAndDay(MealType type, WeekDay day){
        return mealDao.findMeal(type, day);
    }
}
