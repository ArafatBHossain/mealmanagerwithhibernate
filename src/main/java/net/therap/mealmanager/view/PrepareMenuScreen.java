package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealTypeService;
import net.therap.mealmanager.service.WeekDayService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class PrepareMenuScreen {

    private MealTypeService mealTypeService;

    public PrepareMenuScreen(){
        mealTypeService = new MealTypeService();
    }


    public void prepareMealScreen() throws SQLException, ClassNotFoundException {

        UserInterfaceManager.setPrepareMenuScreenUI();

        WeekDay day = takeDayInput();

        printMealTypeList(mealTypeService.getAllMealType());

        MealType type = takeMealTypeInput();

        prepareMealForThisDay(day, type);

    }


    private void prepareMealForThisDay(WeekDay day, MealType type) throws SQLException, ClassNotFoundException {
        DishService dishService = new DishService();

        printAllDishes(dishService.getAllDishes());

        Scanner inputDishScanner = new Scanner(System.in);

        List<Dish> dishToAdd = new ArrayList<>();

        while (true) {
            System.out.print("Enter id to add dishes to menu[Press 0 to stop]: ");
            int dishId = inputDishScanner.nextInt();

            if(dishId==0){
                break;
            }else{
                Dish dish = dishService.getDish(dishId);
                dishToAdd.add(dish);
            }

        }

        Meal meal = new Meal();
        meal.setDay(day);
        meal.setType(type);
        meal.setListOfDishes(dishToAdd);

        MealService mealService = new MealService();
        mealService.saveMeal(meal);

        HomeScreen.setHomeScreen();

    }

    public void printAllDishes(List<Dish> allDishes) {
        for (Dish dish : allDishes) {
            System.out.printf("%20s%15s","["+String.valueOf(dish.getId())+"]",dish.getName()+"\n");
        }

    }


    public MealType takeMealTypeInput() throws SQLException, ClassNotFoundException {
        MealType type = null;
        System.out.print("\n\nSelect Meal Box [0 to go back]: ");

        int inputTypeOfMeal = new Scanner(System.in).nextInt();
        if(inputTypeOfMeal == 0){
            prepareMealScreen();
        }else {
            type =  mealTypeService.getMealType(inputTypeOfMeal);
        }
        return type;
    }


    public WeekDay takeDayInput() throws SQLException, ClassNotFoundException {
        Scanner dayInputScanner = new Scanner(System.in);
        int weekDayId = dayInputScanner.nextInt();

        if (weekDayId == 0) {
            HomeScreen.setHomeScreen();
        }

        WeekDayService weekDayService = new WeekDayService();
        return weekDayService.getWeekDay(weekDayId);
    }


    public void printMealTypeList(List<MealType> mealTypeList) {
        for (MealType mealType : mealTypeList) {
            System.out.printf("%s%22s","["+mealType.getId()+"] ",mealType.getName()+"\n");
        }
    }
}
