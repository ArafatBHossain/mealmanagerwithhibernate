package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.service.DishService;
import java.sql.SQLException;
import java.util.*;

/**
 * @author arafat
 * @since 11/14/16
 */
public class ManageDishesScreen extends PrepareMenuScreen{

    private DishService dishService;

    private String underlineVariableForView = "-----------------------------------------------" +
            "-----------------\n";

    public void manageDishes() throws SQLException, ClassNotFoundException {

        UserInterfaceManager.setManageDishScreenUI(underlineVariableForView);

        String decision = new Scanner(System.in).next();

        if (decision.equals("B")) {
            HomeScreen.setHomeScreen();
        } else {
            dishService = new DishService();
            super.printAllDishes(dishService.getAllDishes());
            takeDishInputFromUser();
        }
    }


    private void takeDishInputFromUser() throws SQLException, ClassNotFoundException {

        while (true) {
            System.out.print("Enter new dish name [Type Q to stop adding and quit]: ");
            Scanner scannerDishNameInput = new Scanner(System.in);
            String dishName = scannerDishNameInput.nextLine();

            if (dishName.equals("Q")) {
                break;
            } else {
                Dish dish = new Dish();
                dish.setName(dishName);
                System.out.println(dish.getName());
                dishService = new DishService();
                dishService.saveDish(dish);
            }
        }

        super.printAllDishes(dishService.getAllDishes());

        HomeScreen.setHomeScreen();
    }
}
