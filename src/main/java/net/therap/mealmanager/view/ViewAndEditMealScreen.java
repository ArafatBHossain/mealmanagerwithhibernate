package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.Dish;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.service.MealService;
import net.therap.mealmanager.service.DishService;
import net.therap.mealmanager.service.MealTypeService;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class ViewAndEditMealScreen extends PrepareMenuScreen {

    private MealService mealService;

    private String underlineVariableForView = "-----------------------------------------------" +
            "-----------------\n";


    public ViewAndEditMealScreen(){
        mealService = new MealService();
    }


    public void viewAndEditMealScreen() throws SQLException, ClassNotFoundException {
        UserInterfaceManager.setViewAndEditMealScreenUI(underlineVariableForView);
        Meal meal = viewMeal();
        editMeal(meal);
    }


    private void editMeal(Meal meal) throws SQLException, ClassNotFoundException {
        System.out.print("[H]Home [N]Continue Viewing [A] Add dishes to menu [D] Delete dishes from menu : ");
        String decisionInput = new Scanner(System.in).next();

        switch (decisionInput) {
            case "H":
                HomeScreen.setHomeScreen();
                break;

            case "N":
                viewAndEditMealScreen();
                break;

            case "D":
                deleteDishFromMenu(meal);
                break;

            case "A":
                addDishToExistingMeal(meal);
                break;
        }
    }


    private Meal viewMeal() throws SQLException, ClassNotFoundException {

        WeekDay day = super.takeDayInput();

        MealTypeService mealTypeService = new MealTypeService();
        super.printMealTypeList(mealTypeService.getAllMealType());

        MealType type = super.takeMealTypeInput();
        Meal meal = mealService.findMealByTypeAndDay(type, day);

        showMenu(meal.getListOfDishes(), meal.getDay().getName());

        return meal;
    }


    public void showMenu(List<Dish> dishList, String weekDay) {
        System.out.printf("%s%40s%s", underlineVariableForView,
                "Menu for " + weekDay+"\n", underlineVariableForView);
        for (Dish dish : dishList) {
            System.out.printf("%20s%15s","["+String.valueOf(dish.getId())+"]",dish.getName()+"\n");
        }
    }


    public void addDishToExistingMeal(Meal meal) throws SQLException,
            ClassNotFoundException {

        DishService dishService = new DishService();
        mealService = new MealService();

        List<Dish> allDishes = dishService.getAllDishes();
        for (Dish dish : allDishes) {
            System.out.printf("%20s%s","["+String.valueOf(dish.getId())+"] ",dish.getName()+"\n");
        }

        List<Dish> listOfDishForThisMeal = meal.getListOfDishes();
        while (true) {
            System.out.print("Enter ID to add dishes to menu (Press 0 to Stop Adding)");

            int dishIdInput = new Scanner(System.in).nextInt();
            if (dishIdInput == 0) {
                break;

            }else{
                Dish dish = dishService.getDish(dishIdInput);
                listOfDishForThisMeal.add(dish);
            }

        }
        mealService.updateMeal(meal);
        showMenu(listOfDishForThisMeal, meal.getDay().getName());
        viewAndEditMealScreen();
    }


    public void deleteDishFromMenu(Meal meal) throws SQLException,
            ClassNotFoundException {
        System.out.print("Select ID to delete: ");
        int dishIdToDelete = new Scanner(System.in).nextInt();
        List<Dish> dishList = meal.getListOfDishes();
        for (int i = 0; i < dishList.size(); i++) {
            Dish dish = dishList.get(i);
            if (dish.getId()==dishIdToDelete) {
                dishList.remove(i);
                i--;
            }
        }
        mealService.updateMeal(meal);

        showMenu(dishList, meal.getDay().getName());
        viewAndEditMealScreen();
    }

}
