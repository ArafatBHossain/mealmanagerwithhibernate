package net.therap.mealmanager.view;

import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.service.MealTypeService;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * @author arafat
 * @since 11/14/16
 */
public class ManageMealTypeScreen {

    private MealTypeService mealTypeService;

    private List<MealType> listOfMealType;

    String underlineVariableForView = "-----------------------------------------------" +
            "-----------------\n";


    public void manageMealType() throws SQLException, ClassNotFoundException {

        UserInterfaceManager.setManageMealTypeScreenUI(underlineVariableForView);

        Scanner sc = new Scanner(System.in);
        String decisionInput = sc.next();

        switch (decisionInput) {
            case "A":
                addMealType();
                break;

            case "V":
                viewMealType();
                break;

            case "B":
                HomeScreen.setHomeScreen();
                break;
        }
    }


    private void viewMealType() throws SQLException, ClassNotFoundException {
        mealTypeService = new MealTypeService();

        listOfMealType = mealTypeService.getAllMealType();
        System.out.printf("%s%40s%s", underlineVariableForView, "Meal types\n",
                underlineVariableForView);

        for (MealType mealType : listOfMealType) {
            System.out.printf("%40s", mealType.getName() + "\n");
        }
        System.out.println();
        manageMealType();
    }


    private void addMealType() throws SQLException, ClassNotFoundException {
        mealTypeService = new MealTypeService();

        System.out.print("Please enter type of meal[B to go to previous menu]: ");

        Scanner scannerInputMealType = new Scanner(System.in);
        String mealTypeName = scannerInputMealType.nextLine();

        if(!mealTypeName.equals("B")){
             mealTypeService.saveMealType(new MealType(mealTypeName));

        }else {
             manageMealType();
        }

        System.out.println();

        viewMealType();
    }
}
