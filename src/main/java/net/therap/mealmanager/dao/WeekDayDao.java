package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.WeekDay;

/**
 * @author arafat
 * @since 11/24/16
 */
public class WeekDayDao extends GenericDaoImpl<WeekDay, Integer> {

    public WeekDayDao(){
        super(WeekDay.class);
    }
}

