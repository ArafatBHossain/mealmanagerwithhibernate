package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.MealType;
import net.therap.mealmanager.domain.WeekDay;
import net.therap.mealmanager.utility.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * @author arafat
 * @since 11/20/16
 */
public class MealDao extends GenericDaoImpl<Meal, Integer> {

    public MealDao(){
        super(Meal.class);
    }

    public Meal findMeal(MealType type, WeekDay day){
        Session session = HibernateUtil.getSessionAnnotationFactory().getCurrentSession();
        Transaction txt = session.beginTransaction();
        Meal meal = null;
        try {
            Query query = session.createQuery("SELECT o FROM " + Meal.class.getName() + " o WHERE" +
                    " o.day = :day AND o.type = :type");

            query.setParameter("day", day);
            query.setParameter("type", type);

            meal = (Meal) query.list().get(0);
            txt.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return meal;
    }

  }
